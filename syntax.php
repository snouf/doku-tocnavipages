<?php
/**
 * DokuWiki Plugin tocnavipages (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Jonas Fourquier <jonas@tuxfamily.org>
 */

if (!defined('DOKU_INC')) die();

class syntax_plugin_tocnavipages extends DokuWiki_Syntax_Plugin {
    var $clear_tocnavipages_for = True;

    public function getType() {
        return 'substition'; //container|baseonly|formatting|substition|protected|disabled|paragraphs
    }

    public function getPType() {
        return 'block'; //normal|block|stack;
    }

    public function getSort() {
        return 300;
    }

    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('<tocnavipages>.*?</tocnavipages>',$mode,'plugin_tocnavipages');
    }

    public function handle($match, $state, $pos, Doku_Handler &$handler){
        global $ID;
        $match = substr($match,15,-16);
        $data = array(
                'toc'=>array(), //le sommaire sous la forme d'une liste
                'from'=>$ID, //page ou se trouve le TOC
                'not_first_page_warning'=>''
            );

        preg_match('%^(.*?)<not_first_page_warning>(.*?)</not_first_page_warning>(.*?)$%is',$match, $matches);
        if ($matches) {
            $infos = array();
            $data['not_first_page_warning'] = p_render('xhtml',p_get_instructions($matches [2]),$infos);
            $match = $matches[1].$matches[3];
        }

        foreach (explode("\n",$match) as $line) {
            // vérifie que c'est un élément de type liste
            $pos = strpos($line, '* ');
            if ($pos === False) //"*" non trouvé
                $pos = strpos($line, '- ');
            if ($pos && ($pos%2 != 1) && !trim(substr($line,0,$pos))) {
                /*
                 * "*" ou "-" :
                 *  - trouvé et pas en 1ère position
                 *  - tabulation correcte
                 *  - n'est que précédé par des espaces
                 */
                $id = False;
                $text = trim($line);
                if ((substr($text,2,2) == '[[') && (substr($text,-2) == ']]')) {
                    // commence par [[ et finit par ]]
                    list($id,$title) = explode('|',substr($text, 4, -2),2);//title = False => title pris dans le titre de la page
                    $id = str_replace('/',':',$id);
                    if ($id{0} != ':') //lien relatif
                        $id = getNS($ID).':'.cleanID($id);//sectionID
                    else
                        $id = cleanID($id);
                    if ($title)
                        $title = htmlspecialchars($title);
                    else
                        $title = p_get_metadata($id,'title');
                    if (!$title)
                        $title = substr($text, max(4,strrpos($text,':',4)+1), -2);
                    if ($id) {
                        $data['toc'][] = array(
                                'id'=>$id,
                                'title'=>$title,
                                'level'=>$pos / 2
                            );
                    }
                }
                if (!$id) {
                    $data['toc'][] = array(
                            'id'=>$ID.'#error',
                            'title'=>'Error: malformated link "'.str_replace(' ','&nbsp;',htmlentities(substr($text,2))).'"',
                            'level'=>$pos / 2
                        );
                }
            } elseif (trim($line)) {
                $data['toc'][] = array(
                        'id'=>$ID.'#error',
                        'title'=>'Error: malformated list "'.str_replace(' ','&nbsp;',htmlentities($line)).'"',
                        'level'=>1
                    );
            }
        }
        return $data;
    }

    public function render($mode, Doku_Renderer &$renderer, $data) {
        global $ID, $ACT;
        if ($mode == 'xhtml') {
            $toc = array();
            foreach ($data['toc'] as $item) {
                $item['type'] = 'ul';
                $toc[] = $item;
            }
            $renderer->doc .= html_buildlist($toc, 'tocnavipages_list', 'tocnavipages_html_buildlist_cb');
            return True;
        } elseif ($mode == 'metadata' && $ACT != 'preview' && !$REV) {
            //1/ on supprime les anciennes entrées
            if ($this->clear_tocnavipages_for) {
                foreach (p_get_metadata($ID,'tocnavipages_for') as $id){
                    p_set_metadata($id, array('tocnavipages'=>array()), False, True);
                }
                $this->clear_tocnavipages_for = False; //evite la purge si plusieur tocnavipages dans une pages
            }

            $for = array();
            //2/ on insére les nouvelles entrées
            foreach ($data['toc'] as $item) {
                $for[] = $item['id'];
                p_set_metadata($item['id'], array('tocnavipages'=>$data), False, True);
            }

            //3/ on définit la liste des pages utilisant ce TOC (pour pouvoir faire 1/)
            p_set_metadata($ID, array('tocnavipages_for'=>$for), False, True);
            return true;
        }
        return false;
    }
}
