<?php
/**
 * DokuWiki Plugin tocnavipages (Action Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Jonas Fourquier <jonas@tuxfamily.org>
 */

if(!defined('DOKU_INC')) die();

function tocnavipages_html_buildlist_cb($item) {
    if ($item['id'])
        return html_wikilink($item['id'], $item['title']);
    elseif ($item['title'])
        return '<span>'.$item['title'].'</span>';
    else
        return '<span class="error">Error: empty item</span>';
}


class action_plugin_tocnavipages extends DokuWiki_Action_Plugin {

    public function register(Doku_Event_Handler $controller) {
       $controller->register_hook('TPL_TOC_RENDER', 'BEFORE', $this, 'handle_tpl_toc_render');
       $controller->register_hook('TPL_CONTENT_DISPLAY', 'BEFORE', $this, 'handle_tpl_content_display');
       $controller->register_hook('IO_WIKIPAGE_WRITE', 'BEFORE', $this, 'handle_io_wikipage_write');
    }

    public function handle_tpl_toc_render(Doku_Event &$event, $param) {
        global $ID;
        $meta = p_get_metadata($ID,'tocnavipages');
        if (isset($meta['toc']) && !empty($meta['toc'])) {
            $newtoc = array();
            foreach ($meta['toc'] as $item) {
                if ($item['id'] == $ID) {
                    //C'est la page en cours
                    if (empty($event->data)) {
                        //toc vide
                        $newtoc[] = array(
                                'hid' => '#',
                                'title' => $item['title'],
                                'type' => 'ul',
                                'level' => $item['level']
                            );
                    }
                    foreach ($event->data as $item_curpage) {
                        //toc plein
                        $newtoc[] = array(
                                'hid' => $item_curpage['hid'],
                                'title' => $item_curpage['title'],
                                'type' => 'ul',
                                'level' => $item['level'] + $item_curpage['level'] - 1
                            );
                    }
                } else {
                    // autre pages
                    $newtoc[] = array(
                                'link' => wl($item['id']),
                                'title' => $item['title'],
                                'type' => 'ul',
                                'level' => $item['level']
                            );
                }
            }
            $event->data = $newtoc;
        }
    }

    public function handle_tpl_content_display(Doku_Event &$event, $param) {
        global $ID, $ACT;
        $meta = p_get_metadata($ID,'tocnavipages');
        $page_exists = page_exists($ID);
        if (isset($meta['toc']) && !empty($meta['toc'])) {
            //navbar
            $prev = False;
            $next = False;
            $cur = False;
            $pageswitcher = '<form action="#"><select class="edit" class="tocnavipage_pageswitcher">';
            $idstoc = array();
            foreach ($meta['toc'] as $item) {
                $idstoc[] = $item['id'];
                if ($ID == $item['id'])
                    $cur = $item;
                elseif (!$cur)
                    $prev = $item;
                elseif (!$next && $cur)
                    $next = $item;
                $pageswitcher .= '<option value="'.wl($item['id']).'"'.($item['id'] == $ID ? ' selected="selected"' : '').'>'.str_repeat('&nbsp;',($item['level']-$meta['toc'][0]['level'])*2).$item['title'].'</option>';
            }
            $pageswitcher .= '</select></form>';
            //include toc
            if (!$page_exists || strpos($event->data,'~~TOCNAVIPAGES~~')) {
                $subtoc = array();
                foreach ($meta['toc'] as $subitem) {
                    if (isset($parent_subtoc) && ($subitem['level'] <= $parent_subtoc['level']))
                        break;
                    elseif (isset($parent_subtoc) && ($subitem['level'] > $parent_subtoc['level']))
                        $subtoc[] = array(
                                'id'=>$subitem['id'],
                                'title'=>$subitem['title'],
                                'level'=>$subitem['level'] - $parent_subtoc['level'],
                                'type'=>'ul'
                            );
                    elseif ($subitem['id'] == $ID)
                        $parent_subtoc = $subitem;
                }
                if (!empty($subtoc))
                    $html_subtoc = html_buildlist($subtoc, 'tocnavipages_list', 'tocnavipages_html_buildlist_cb');
                else
                    $html_subtoc = '';
            }

            //for not_first_page_warning
            $crumbs = breadcrumbs();
            $id_referer = (count($crumbs) > 1) ? key(array_slice(breadcrumbs(), -2,1,True)) : False;

            if ($ACT == 'show') {
                $strpos_toc_end = strpos($event->data, '<!-- TOC END -->');
                if ($strpos_toc_end) $strpos_toc_end+=16;
                $event->data = '
                    <div class="tocnavipages_nav tocnavipages_nav_head">
                        <div class="prev">'.($prev ? html_wikilink(':'.$prev['id'], '⇦ '.$prev['title']) : '<!-- is first page -->').'</div>
                        <div class="cur">
                            <!--<span>'.$cur['title'].'</span>-->
                            '.$pageswitcher.'
                        </div>
                        <div class="next">'.($next ? html_wikilink(':'.$next['id'], $next['title'].' ⇨') : '<!-- is last page -->').'</div>
                    </div>
                    <div class="secedit editbutton_tocnavipages">
                    '.html_btn('secedit',$meta['from'],false,false).'
                    </div>
                    '.(( (!in_array($id_referer,$idstoc)) && ($ID != $idstoc[0] ))
                        ? '<div class="tocnavipages_notfirst">'.$meta['not_first_page_warning'].'</div>'
                        : '').'
                    '.($html_subtoc
                        ? ($page_exists
                            ? str_replace('~~TOCNAVIPAGES~~', $html_subtoc, $event->data)
                            : substr($event->data, 0,$strpos_toc_end).' <h1>'.$parent_subtoc['title'].'</h1> '.$html_subtoc)
                        : $event->data).'
                    <div class="tocnavipages_nav tocnavipages_nav_foot">
                        <div class="prev">'.($prev ? html_wikilink(':'.$prev['id'], '⇦ '.$prev['title']) : '<!-- is first page -->').'</div>
                        <div class="cur">
                            <!--<span>'.$cur['title'].'</span>-->
                            '.$pageswitcher.'
                        </div>
                        <div class="next">'.($next ? html_wikilink(':'.$next['id'], $next['title'].' ⇨') : '<!-- is last page -->').'</div>
                    </div>';
            }
        }
    }

    public function handle_io_wikipage_write(Doku_Event &$event, $param) {
        global $ID;
        $title = p_get_metadata($ID,'title');
        if (p_get_metadata($ID,'title') != $event->data[2]) {
            //Le nom de la page à changer, on efface le cache des metas de la source
            p_set_metadata(p_get_metadata($ID,'tocnavipages')['from'], array('cache' => 'expire'), False, False); //FIXME est-ce la bonne méthode ?
        }
    }

}
